import time
import boto3
import requests
import os
import sys
from requests.exceptions import ConnectTimeout


jenkins_ip_address = sys.argv[1]

client = boto3.client('cloudwatch')

while(True):
    try:
        is_healthy = 1 if requests.get("http://" + jenkins_ip_address + ":8080/login", timeout=10).status_code == 200 else 0
    except ConnectTimeout:
        is_healthy = 0
    print(is_healthy)
    client.put_metric_data(
        Namespace='Jenkins',
        MetricData=[
            {
                'MetricName': 'healthy',
                'Dimensions': [
                    {
                        'Name': 'hostname',
                        'Value': jenkins_ip_address
                    },
                ],
                'Value': is_healthy,
                'Unit': 'Count'
            },
        ]
    )
    time.sleep(60)
